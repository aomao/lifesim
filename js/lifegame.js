/*!
 * Life Game Simulator script
 */
 (function ($) {
//Global vars
var skills = 1;
var experience = 1;
var knowledge = 1;
var money = 100;

var health = 80;
var hapiness = 50;

var eatingStatus = false;
var lovingStatus = false;
var learningStatus = false;
var workingStatus = false;
var exercisingStatus = false;
var travelingStatus = false;

var familyAch = true;
var businessAch = true;
var houseAch = true;
var unlockSkilledWorker = true;

var boyAlive = true;

var doingSeconds = 0;

$( "#draggable" ).draggable({ cursor: "move", containment: "parent" });

$("#eat").droppable({
               drop: function (event, ui) {
               $( this )
               .addClass( "highlight" )
               .find( "p" ).html( "Eating..." );
               eatingStatus = true;
               doingSeconds = 0;
               eating();
               },   
               over: function (event, ui) {
               $( this )
               .addClass( "highlight" );
               },
               out: function (event, ui) {
               $( this )
               .removeClass( "highlight" )
               .find( "p" ).html( "Eat" );
               eatingStatus = false;
               }      
            });

$("#love").droppable({
               drop: function (event, ui) {
               $( this )
               .addClass( "highlight" )
               .find( "p" ).html( "Loving..." );
               lovingStatus = true;
               doingSeconds = 0;
               loving();
               },   
               over: function (event, ui) {
               $( this )
               .addClass( "highlight" );
               },
               out: function (event, ui) {
               $( this )
               .removeClass( "highlight" )
               .find( "p" ).html( "Love" );
               lovingStatus = false;
               }      
            });

$("#learn").droppable({
               drop: function (event, ui) {
               $( this )
               .addClass( "highlight" )
               .find( "p" ).html( "Learning..." );
               learningStatus = true;
               doingSeconds = 0;
               learning();
               },   
               over: function (event, ui) {
               $( this )
               .addClass( "highlight" );
               },
               out: function (event, ui) {
               $( this )
               .removeClass( "highlight" )
               .find( "p" ).html( "Learn" );
               learningStatus = false;
               }      
            });

$("#work").droppable({
               drop: function (event, ui) {
               $( this )
               .addClass( "highlight" )
               .find( "p" ).html( "Working..." );
               workingStatus = true;
               doingSeconds = 0;
               working();
               },   
               over: function (event, ui) {
               $( this )
               .addClass( "highlight" );
               },
               out: function (event, ui) {
               $( this )
               .removeClass( "highlight" )
               .find( "p" ).html( "Work" );
               workingStatus = false;
               }      
            });

$("#exercise").droppable({
               drop: function (event, ui) {
               $( this )
               .addClass( "highlight" )
               .find( "p" ).html( "Exercising..." );
               exercisingStatus = true;
               doingSeconds = 0;
               exercising();
               },   
               over: function (event, ui) {
               $( this )
               .addClass( "highlight" );
               },
               out: function (event, ui) {
               $( this )
               .removeClass( "highlight" )
               .find( "p" ).html( "Exercise" );
               exercisingStatus = false;
               }      
            });

$("#travel").droppable({
               drop: function (event, ui) {
               $( this )
               .addClass( "highlight" )
               .find( "p" ).html( "Traveling..." );
               travelingStatus = true;
               doingSeconds = 0;
               traveling();
               },   
               over: function (event, ui) {
               $( this )
               .addClass( "highlight" );
               },
               out: function (event, ui) {
               $( this )
               .removeClass( "highlight" )
               .find( "p" ).html( "Travel" );
               travelingStatus = false;
               }      
            });


var eating = function(){
    if(!eatingStatus){
        // stop doing this action
        //console.log( "eating stopped");
    }
    else {
        //he is still doing this action
        setTimeout(eating, 3000); // check again in a second
        //console.log( "eating...");
        doingSeconds++;
        changeMoney(-1);
        if(doingSeconds<3){
        	changeHealth(2);
        	changeHappiness(1);
        }else{
        	changeHealth(-2);
        }
    }
};

var loving = function(){
    if(!lovingStatus){
        // stop doing this action
        //console.log( "loving stopped");
    }
    else {
        //he is still doing this action
        setTimeout(loving, 3000); // check again in a second
        //console.log( "loving...");
        doingSeconds++;
        if(doingSeconds<6){
        	changeHealth(1);
        	changeHappiness(3);
        }else{
        	changeHealth(-2);
        }
    }
};

var learning = function(){
    if(!learningStatus){
        // stop doing this action
        //console.log( "learning stopped");
    }
    else {
        //he is still doing this action
        setTimeout(learning, 3000); // check again in a second
        //console.log( "loving...");
        doingSeconds++;
        changeMoney(-1);
        if(doingSeconds<5){
        	changeKnowledge(2);
        	changeSkills(1);
        }else{
        	changeHealth(-2);
        	changeHappiness(-1);
        }
    }
};

var working = function(){
    if(!workingStatus){
        // stop doing this action
        //console.log( "working stopped");
    }
    else {
        //he is still doing this action
        setTimeout(working, 3000); // check again in a second
        //console.log( "loving...");
        doingSeconds++;
        changeSkills(1);
        changeMoney(3);
        changeExperience(2);
        if(doingSeconds<10){
        	       	
        }else{
        	changeHealth(-2);
        	changeHappiness(-1);
        }
    }
};

var exercising = function(){
    if(!exercisingStatus){
        // stop doing this action
        //console.log( "exercising stopped");
    }
    else {
        //he is still doing this action
        setTimeout(exercising, 3000); // check again in a second
        //console.log( "exercising...");
        doingSeconds++;
        if(doingSeconds<10){
        	changeHealth(3);
        	changeHappiness(1);
        }else{
        	changeHealth(-2);
        	changeHappiness(-1);
        }
    }
};

var traveling = function(){
    if(!travelingStatus){
        // stop doing this action
        //console.log( "traveling stopped");
    }
    else {
        //he is still doing this action
        setTimeout(traveling, 3000); // check again in a second
        //console.log( "traveling...");
        doingSeconds++;
        changeMoney(-2);
        changeKnowledge(1);
        if(doingSeconds<10){
        	changeHealth(1);
        	changeHappiness(2);       	
        }else{
        	changeHealth(-2);
        	changeHappiness(-1);
        }
    }
};

/*Life status changes*/
var changeHealth = function(value){
	var curValue = parseInt($('#healthProgr').attr('aria-valuenow'));
	curValue = curValue + value;
	
	if (curValue<=0) {
		boyDies();
	}else if(curValue>100){
		curValue=100;
	}else{
		curValue = curValue;
	}
	$('#healthProgr').attr('aria-valuenow',curValue);
	$('#healthProgr').attr('style', 'width:'+curValue+'%;');
	$('#healthProgr').text(curValue+'%');
};

var changeHappiness = function(value){
	var curValue = parseInt($('#happyProgr').attr('aria-valuenow'));
	curValue = curValue + value;
	
	if(curValue>50){
		getAchievements();
	}
	if(curValue>100){
		curValue=100;
	}else if (curValue<=0) {
		boyDies();
	}
	$('#happyProgr').attr('aria-valuenow',curValue);
	$('#happyProgr').attr('style', 'width:'+curValue+'%;');
	$('#happyProgr').text(curValue+'%');
};

/*Inventory changes*/
var changeSkills = function(value){
	if(boyAlive){
		var curValue = parseInt($('#inskills').text());
		curValue = curValue + value;
		$('#inskills').text(curValue);
	}
};
var changeExperience = function(value){
	if(boyAlive){
	var curValue = parseInt($('#inexperience').text());
	curValue = curValue + value;
	$('#inexperience').text(curValue);
}
};
var changeKnowledge = function(value){
	if(boyAlive){
	var curValue = parseInt($('#inknowledge').text());
	curValue = curValue + value;
	$('#inknowledge').text(curValue);
}
};
var changeMoney = function(value){
	if(boyAlive){
	var curValue = parseInt($('#inmoney').text());
	var knowledge = parseInt($('#inknowledge').text());
	var experience = parseInt($('#inexperience').text());
	var skills = parseInt($('#inskills').text());
	if(knowledge>25 && experience>25 && skills>25){
		//you are now specialist, which gets bigger salary
		if (unlockSkilledWorker) {
			$('#ach_skilled_worker').show();
			$('#notifyAudio')[0].play();
			unlockSkilledWorker = false;
		};
		
		curValue = curValue + (value*2);
	}else{
		curValue = curValue + value;
	}
	
	$('#inmoney').text(curValue);
	if(curValue>150){
		getAchievements();
	}
}
};

/*Get Achievements*/
var getAchievements = function(){
	var curSkills = parseInt($('#inskills').text());
	var curExperience = parseInt($('#inexperience').text());
	var curKnowledge = parseInt($('#inknowledge').text());
	var curMoney = parseInt($('#inmoney').text());
	var curHappiness = parseInt($('#happyProgr').attr('aria-valuenow'));
	
	if(curMoney>149 && curHappiness>49 && curKnowledge>24){
		if(familyAch){
			//get family
			$('#ach_family').show();
			$('#ach_family_locked').hide();
			$('#notifyAudio')[0].play();
			familyAch = false;
		}
		
	}
	if(curMoney>199 && curKnowledge>49 && curSkills>29 && curExperience>29 && curHappiness>49){
		if(businessAch){
			//get business
			$('#ach_business').show();
			$('#ach_business_locked').hide();
			$('#notifyAudio')[0].play();
			businessAch = false;
		}
	}
	if(curMoney>299 && curKnowledge>59 && curSkills>59 && curExperience>59 && curHappiness>49){		
		if(houseAch){
			//get house
			$('#ach_house').show();
			$('#ach_house_locked').hide();
			$('#notifyAudio')[0].play();
			houseAch = false;
		}
	}
};

var boyDies = function () {
	$('#draggable').addClass('die');
	$('#statusbox').html('<h2>Game Over</h2>');
	$('#draggable').draggable( "disable" );
	$('#dieAudio')[0].play();
	boyAlive = false;
};

var doingNothing = function() {
	
	setTimeout(function() {
    if (timeCounter++ < 200) {
      //console.log( "time " + timeCounter );
      doingNothing();
      decrementLifeStatus();
    }
  	}, 3000);
};

var decrementLifeStatus = function() {
	
	//if not doing anything
	if(!eatingStatus&&!lovingStatus&&!learningStatus&&!workingStatus&&!exercisingStatus&&!travelingStatus){
		//console.log( "decrementLifeStatus" );
		changeMoney(-1);
		changeHealth(-1);
		changeHappiness(-1);
	}
};

//initialize doingNothing
//time loop (simulate time passing)
var timeCounter = 0;
doingNothing();


 })(jQuery);